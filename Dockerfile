FROM node:lts-alpine
WORKDIR /app
USER node
COPY --chown=node:node . .
EXPOSE 3000
CMD ["npm", "run", "start"]