def TOKEN = ""
// Set the tag for the development image: version + build number
def BUILD      = "0"

pipeline {
  agent {

    kubernetes {
      yaml """
        apiVersion: v1
        kind: Pod
        metadata:
          labels:
            job: dso.openshift.nodejs
        spec:
          serviceAccount: jenkins
          serviceAccountName: jenkins
          containers:
          - name: git
            image: alpine/git:latest
            command: ["cat"]
            tty: true
          - name: node
            image: node:lts-alpine
            command: ["cat"]
            tty: true
          - name: sonar-scanner
            image: sonarsource/sonar-scanner-cli:4
            command: ["cat"]
            tty: true
          - name: buildah
            image: quay.io/buildah/stable:latest
            command:
            - cat
            tty: true
            env:
            - name: STORAGE_DRIVER
              value: vfs
          - name: oc
            image: image-registry.openshift-image-registry.svc:5000/openshift/cli:latest
            command:
            - cat
            tty: true
        """.stripIndent()
    }

}

  stages {
    stage('environment') {
      steps {
        container('git') {
          script {
            env.revision = sh(script: 'git log -1 --format=\'%h.%ad\' --date=format:%Y%m%d-%H%M | cat', returnStdout: true).trim()
            env.branch = env.BRANCH_NAME.take(20).replaceAll('/', '_')
            if (env.branch != 'master') {
              env.revision += "-${branch}"
            }
            sh "echo Building revision: ${revision}"
            
            // Set the tag for the development image: version + build number 
            BUILD  = currentBuild.number
          }
        }
      }
    }

    stage('dependencies') {
      steps {
        container('node') {
          sh "npm install"
        }
      }
    }

    stage('compile') {
      steps {
        container('node') {          
          sh "npm build"
        }
      }
    }

    stage('unit test') {
      steps {
        container('node') {
          /*
          sh "npm test"
          */
        }
      }

      /*
      post {
        always {
          junit 'test-results.xml'
        }
      }
      */
    }

    stage('sonarqube') {
      when {
        not {
          changeRequest()
        }
      }
      steps {
        sh "echo ''"
        /*
        container('sonar-scanner') {
          withSonarQubeEnv('sonarcloud') {
            sh "sonar-scanner \
                  -Dsonar.branch.name=${BRANCH_NAME}"
          }
          timeout(time: 1, unit: 'HOURS') {
            waitForQualityGate abortPipeline: true
          }
        }
        */
      }
    }

    stage('sonarqube pr') {
      when {
        changeRequest()
      }
      steps {
        sh "echo ''"
        /*
        container('sonar-scanner') {
          withSonarQubeEnv('sonarcloud') {
            script {
              sh "sonar-scanner \
                    -Dsonar.pullrequest.bitbucketcloud.owner={aa927ef2-7f73-41ca-9b03-3a4817d85ca9} \
                    -Dsonar.pullrequest.bitbucketcloud.repository=dso.openshift.nodejs \
                    -Dsonar.pullrequest.base=${CHANGE_TARGET} \
                    -Dsonar.pullrequest.key=${CHANGE_ID} \
                    -Dsonar.pullrequest.branch=${CHANGE_BRANCH}"
            }
          }
          timeout(time: 1, unit: 'HOURS') {
            waitForQualityGate abortPipeline: true
          }
        }
        */
      }
    }

    stage('build artifact') {           
      steps {
        container('oc') {
          script {
            env.TOKEN = sh (script: 'echo $(oc describe secret/jenkins-token-hwxv9 | grep \"token:\" | awk \'{print $2}\')', returnStdout: true).trim()            
          }
        }
        container('buildah') {
          script {
            sh """         
              buildah bud -t devsecops_openshift_nodejs .
              buildah login --tls-verify=false -u jenkins -p ${env.TOKEN} default-route-openshift-image-registry.apps.devsecops.zkhj.p1.openshiftapps.com
              buildah tag devsecops_openshift_nodejs default-route-openshift-image-registry.apps.devsecops.zkhj.p1.openshiftapps.com/devops/devsecops_openshift_nodejs:${revision}
              buildah push --tls-verify=false default-route-openshift-image-registry.apps.devsecops.zkhj.p1.openshiftapps.com/devops/devsecops_openshift_nodejs:${revision}
            """
          }
        }
      }
    }
    
    stage("Deploy to Openshift DEV Project"){
            steps{
                container('oc'){
                    script{
                        openshift.withCluster(){
                            openshift.withProject('devops'){
                                
                                //Blue-Green Option                                
                                echo "Create app nodejs${BUILD}..."
                                def result=openshift.raw("new-app","--docker-image=image-registry.openshift-image-registry.svc:5000/devops/devsecops_openshift_nodejs:${revision}","--name=nodejs${BUILD}","--as-deployment-config")
                                echo "${result.out}"
                                sleep 5
                                def latestDeploymentVersion = openshift.selector('dc',"nodejs${BUILD}").object().status.latestVersion
                                def rc = openshift.selector('rc', "nodejs${BUILD}-${latestDeploymentVersion}").object()
                                while(rc.status.replicas!=rc.status.readyReplicas){
                                    echo "Waiting to be ready and running..."
                                    echo "Defined Replicas: ${rc.status.replicas} Ready Replicas: ${rc.status.readyReplicas}"
                                    rc = openshift.selector('rc', "nodejs${BUILD}-${latestDeploymentVersion}").object()
                                }
                                echo "Validating the service..."
                                if(!openshift.selector("svc","nodejs${BUILD}").exists()){
                                    echo "Service nodejs${BUILD} is not created..."
                                    echo "Creating nodejs${BUILD} service..."
                                    result=openshift.raw("expose","dc","nodejs${BUILD}","--target-port=3000","--port=3000")
                                    echo "${result.out}"
                                }
                                def connected=openshift.verifyService("nodejs${BUILD}")
                                if (connected) {
                                    echo "Able to connect to nodejs${BUILD} service"
                                }
                                else {
                                    echo "Unable to connect to nodejs${BUILD} service"
                                }
                            }
                        }
                    }
                }
            }
        }
        stage("Expose the application?"){
            steps{
                timeout(time: 24, unit:'HOURS'){
                    input message: "Expose the nodejs${BUILD}-application?", ok: "Expose"
                }
                container('oc'){
                    script{
                        openshift.withCluster(){
                            openshift.withProject('devops'){                                
                                //Blue-Green Option
                                echo "Validate the route"
                                if(!openshift.selector("route","nodejs").exists()){
                                    echo "Create route nodejs..."
                                    def result=openshift.raw("expose","service","nodejs${BUILD}","--name=nodejs", "--hostname=nodejs-devops.apps.devsecops.zkhj.p1.openshiftapps.com")
                                    echo "${result.out}"
                                    env.IS_NEW_DEPLOYMENT=true
                                }
                                else{
                                    echo "Updating the route nodejs..."
                                    def route=openshift.selector("route","nodejs").object()
                                    env.OLD_SERVICE=route.spec.to.name
                                    route.spec.to.name="nodejs${BUILD}"
                                    openshift.apply(route)
                                    env.IS_NEW_DEPLOYMENT=false
                                }
                            }
                        }
                    }
                }
            }
        }
        stage("Rollback?"){
            when{
                environment name: 'IS_NEW_DEPLOYMENT', value: 'false'
            }
            steps{
                timeout(time: 24, unit:'HOURS'){
                    input message: "Rollback the nodejs application?", ok: "Rollback"
                }
                container('oc'){
                    script{
                        openshift.withCluster(){
                            openshift.withProject('devops'){
                                /* Rollout Option

                                */
                                //Blue-Green Option
                                echo "Updating the route nodejs..."
                                def route=openshift.selector("route","nodejs").object()
                                route.spec.to.name=OLD_SERVICE
                                openshift.apply(route)
                                env.IS_ROLLBACKED=true
                            }
                        }
                    }
                }
            }
            post{
                success{
                    container('oc'){
                        script{
                            openshift.withCluster(){
                                openshift.withProject('devops'){
                                    echo "Cleanning Environment"
                                    echo "Cleanning the nodejs-${BUILD} objects"
                                    def result=openshift.raw("delete","all","-l","app=nodejs${BUILD}")
                                    echo "${result.out}"
                                }
                            }
                        }
                    }
                }
                aborted{
                    container('oc'){
                        script{
                            openshift.withCluster(){
                                openshift.withProject('devops'){
                                    echo "Cleanning Environment"
                                    echo "Cleanning the ${OLD_SERVICE} objects"
                                    def result=openshift.raw("delete","all","-l","app=${OLD_SERVICE}")
                                    echo "${result.out}"
                                }
                            }
                        }
                    }
                }
            }
        }
         
    }
}